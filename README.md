# Huffman encoding challenge

See https://codingchallenges.fyi/challenges/challenge-huffman/ for the details.

## Running

To run the program on the sample file, use the following command to encode:

`./gradlew run -q --args="-e /Users/martinvanvliet/Dev/Code/CodingChallenges/huffman/les_miserables.txt les_miserables.compress"`

Note that the output file ends up in the `app` directory.

To decode, use:

`./gradlew run -q --args="-d les_miserables.compress les_miserables_decoded.txt"`

When things work properly, the following command shows no differences:

`diff les_miserables.txt app/les_miserables_decoded.txt`

## TODO

* Check the code into your personal github
