package huffman;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Encoder implements FileConstants {
    private Logger log = LoggerFactory.getLogger(this.getClass().getName());

    private File outFile;

    private File inFile;

    public Encoder(File inFile, String outFile) throws IOException {
        this.inFile = inFile;
        this.outFile = new File(outFile);
    }

    // For testing
    protected Encoder() {}

    public void encode() throws IOException {
        Tree tree = new Tree(new FrequencyTable(inFile), FileConstants.END_OF_FILE_MARKER);        
        PrefixCodeTable pt = new PrefixCodeTable(tree);
        List<CharEncoding> codes = getCharacterEncodings(pt);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(outFile);
            fos.write(MAGIC_NUMBER);

            log.debug("Writing tree to compressed file, weight = " + tree.getWeight());
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            tree.writeToStream(oos);
            oos.flush();

            encodeAndWrite(codes, fos);
            fos.flush();

            fos.close();
        } catch(Exception e) {
            if (fos != null) {
                fos.close();
            }
            throw e;
        }
    }

    protected void encodeAndWrite(List<CharEncoding> codes, OutputStream os) throws IOException {
        byte[] buffer = new byte[BUF_SIZE];
        encodeAndWrite(codes, buffer, os);
    }

    protected void encodeAndWrite(List<CharEncoding> codes, byte[] buffer, OutputStream os) throws IOException {
        int bufIdx = 0;
        int bitIdx = 7;

        // Loop over character encodings & pack them into bytes
        for (CharEncoding c : codes) {
            // log.debug("  Writing encoded {}", c.c);
            for (int i = 0; i < c.length(); i++) {
                if (c.get(i)) {
                    // Start filling the byte from the left side
                    buffer[bufIdx] |= (1 << bitIdx);
                }
                bitIdx--;
                if (bitIdx < 0) {
                    bitIdx = 7;
                    bufIdx++;

                    // If we reached the end of the buffer, write it to outputstream
                    if (bufIdx == BUF_SIZE) {
                        log.debug("Reached end of output buffer, writing {} bytes to file", buffer.length);
                        if (os != null) {
                            os.write(buffer);
                        }
                        Arrays.fill(buffer, (byte) 0);
                        bufIdx = 0;
                    }
                }
            }
        }
        log.debug("Reached end of encodings, writing {} bytes to file", bufIdx);
        if (os != null) {
            // Write part of the buffer that is filled
            os.write(buffer, 0, bufIdx+1);
        }
    }

    private List<CharEncoding> getCharacterEncodings(PrefixCodeTable pt) throws IOException {
        String text = Files.readString(inFile.toPath());
        List<CharEncoding> codes = new ArrayList<>();

        long encodedBits = 0;
        for (int i = 0; i < text.length(); i++) {
            char charAt = text.charAt(i);
            CharEncoding e = pt.get(charAt);
            encodedBits += e.length();
            codes.add(e);
        }
        // Write end of file marker
        CharEncoding e = pt.get((char) FileConstants.END_OF_FILE_MARKER);
        log.debug("End of file marker encoded length: {}", e.length());
        codes.add(e);

        log.debug("Expected encoded length: " + (encodedBits/8) + " bytes");
        return codes;
    }

}
