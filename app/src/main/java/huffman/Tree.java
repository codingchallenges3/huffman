package huffman;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Tree implements Serializable {
    private Logger log = LoggerFactory.getLogger(this.getClass().getName());

    TreeNode root = null;

    public Tree(FrequencyTable table) {
        this(table, null);
    }

    public Tree(FrequencyTable table, Integer endOfFileMarker) {
        PriorityQueue<TreeNode> pq = new PriorityQueue<>(Comparator.comparingInt(TreeNode::getWeight));

        // Create leaf nodes
        for (char c : table.keySet()) {
            pq.add(new Leaf(c, table.get(c)));
        }

        // Tests run without EOF marker
        if (endOfFileMarker != null) {
            pq.add(new Leaf((char) endOfFileMarker.intValue(), 1)); // used to recognize the end of the file
            log.debug("Added end of file marker");
        }
        log.debug("Priority Queue size: " + pq.size());

        while (root == null) {
            if (pq.size() > 1) {
                TreeNode left = pq.remove();
                TreeNode right = pq.remove();
                pq.add(new Node(left, right));
            } else {
                root = pq.remove();
            }
        }
    }

    public void writeToStream(ObjectOutputStream os) throws IOException {
        os.writeObject(this);
    }

    public int getWeight() {
        return root.getWeight();
    }

}

interface TreeNode extends Serializable {
    int getWeight();
    default boolean isLeaf() { return false; }
    String toString(PrefixCodeTable pt);
}

class Node implements TreeNode {
    public TreeNode left;
    public TreeNode right;
    public int weight;
    public Node(TreeNode left, TreeNode right) {
        this.left = left;
        this.right = right;
        this.weight = left.getWeight() + right.getWeight();
    }
    @Override
    public int getWeight() {
        return weight;
    }
    @Override
    public String toString(PrefixCodeTable pt) {
        return "[" + left.toString(pt) + "]  -  [" + right.toString(pt) + "]";
    }
}

class Leaf implements TreeNode {
    public int weight;
    public char c;

    public Leaf(char c, int weight) {
        this.weight = weight;
        this.c = c;
    }

    @Override
    public int getWeight() {
        return weight;
    }
    
    @Override
    public boolean isLeaf() {
        return true;
    }
    @Override
    public String toString(PrefixCodeTable pt) {
        return "{" + c + "," + weight + ", " + pt.get(c).length() + "b}";
    }
}

// Walk the tree and create a bitset for each leaf
class TreeWalker {
    public Map<Character, CharEncoding> collector = new HashMap<>();

    public TreeWalker(TreeNode root) {
        walk(root, new CharEncoding(), 0);
    }

    public void walk(TreeNode node, CharEncoding set, int depth) {
        if (node.isLeaf()) {
            CharEncoding ce = (CharEncoding) set.clone();
            char c = ((Leaf) node).c;
            ce.setC(c);
            collector.put(c, ce);
        } else {
            CharEncoding ce = (CharEncoding) set.clone();

            // left first
            ce.clear(depth);
            walk(((Node)node).left, ce, depth+1);

            // right
            ce.set(depth);
            walk(((Node)node).right, ce, depth+1);
        }
    }
}
