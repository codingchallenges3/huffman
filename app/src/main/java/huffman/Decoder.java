package huffman;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Decoder implements FileConstants {
    private Logger log = LoggerFactory.getLogger(this.getClass().getName());

    private File outFile;

    private File inFile;

    public Decoder(File inFile, String outFile) throws IOException {
        this.inFile = inFile;
        this.outFile = new File(outFile);
    }

    // For testing
    protected Decoder() {}

    public void decode() throws Exception {
        FileInputStream fis = null;
        BufferedWriter bw = null;
        try {
            fis = new FileInputStream(inFile);
            int magic = fis.read();
            if (magic != MAGIC_NUMBER) {
                throw new RuntimeException("Magic number not found, file is not compressed by this tool");
            }

            Tree tree = (Tree) new ObjectInputStream(fis).readObject();
            log.debug("Loaded tree from compressed file, weight = " + tree.getWeight());

            bw = new BufferedWriter(new FileWriter(outFile));
            decodeAndWrite(tree, fis, bw);

            bw.close();
            fis.close();
        } catch(Exception e) {
            if (fis != null) {
                fis.close();
            }
            if (bw != null) {
                bw.close();
            }
            throw e;
        }
    }

    protected void decodeAndWrite(Tree tree, InputStream is, BufferedWriter bw) throws IOException {
        byte[] readBuffer = new byte[BUF_SIZE];
        int read = is.read(readBuffer);
        TreeNode node = tree.root;

        while(read > 0) {
            log.debug("Read {} bytes", read);
            node = processBuffer(tree.root, node, bw, readBuffer, read);
            read = is.read(readBuffer);
        }
    }

    private TreeNode processBuffer(TreeNode root, TreeNode node, BufferedWriter bw, byte[] readBuffer, int read) throws IOException {
        int bufIdx = 0;
        int bitIdx = 7;

        log.debug("Start processing {} bytes of input", read);
        while(bufIdx < read) {
            // Every iteration through the loop processes 1 bit
            int i = readBuffer[bufIdx] & (byte) (1 << bitIdx);
            if (i == 0) {
                node = ((Node) node).left;
            } else {
                node = ((Node) node).right;
            }

            // Have we reached a leaf node? Write the character & reset to root of tree
            if (node.isLeaf()) {
                char c = ((Leaf) node).c;
                
                // Found EOF marker? Then break the loop & stop writing
                if (FileConstants.END_OF_FILE_MARKER == c) {
                    log.debug("Found EOF marker");
                    break;
                } else {
                    bw.append(c);
                    // log.debug("Wrote char {}", c);
                }
                node = root;
            }

            bitIdx--;
            // End of the current byte?
            if (bitIdx < 0) {
                bitIdx = 7;
                // log.debug("Processed byte {} of input", bufIdx);
                bufIdx++;
            }
        }
        bw.flush();
        return node;
    }
}
