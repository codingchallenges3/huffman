package huffman;

import java.util.BitSet;

public class CharEncoding extends BitSet {
    public int max = -1;
    public char c;

    @Override
    public void set(int bitIndex) {
        super.set(bitIndex);
        max = (max > bitIndex ? max : bitIndex);
    }
    @Override
    public void set(int bitIndex, boolean value) {
        super.set(bitIndex, value);
        max = (max > bitIndex ? max : bitIndex);
    }
    @Override
    public void clear(int bitIndex) {
        super.clear(bitIndex);
        max = (max > bitIndex ? max : bitIndex);
    }
    @Override
    public int length() {
        return max + 1;
    }
    public void setMax(int max) {
        this.max = max;
    }
    public void setC(char c) {
        this.c = c;
    }
}