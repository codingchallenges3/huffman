package huffman;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Table that maintains a count of characters.
 */
public class FrequencyTable {
    private Logger log = LoggerFactory.getLogger(this.getClass().getName());

    private Map<Character, Integer> table = new HashMap<Character, Integer>();

    public FrequencyTable(List<String> lines) {
        for (String l : lines) {
            l.chars().forEach(x -> table.put((char) x, table.getOrDefault((char) x, 0) + 1));
        }
        log.debug("Frequency table entries: " + table.size());
    }

    public FrequencyTable(File file) throws IOException {
        this(Files.readAllLines(file.toPath()).stream().map(x -> x + "\n").toList());
    }

    public Integer get(char c) {
        return table.get(c);
    }

    public Set<Character> keySet() {
        return table.keySet();
    }
}
