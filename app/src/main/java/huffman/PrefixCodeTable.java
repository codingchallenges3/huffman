package huffman;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Table mapping a character to a particular encoding in bits according to the Huffman algorithm.
 */
public class PrefixCodeTable implements Serializable {

    private Map<Character, CharEncoding> table = null;

    public PrefixCodeTable(Tree tree) {
        TreeNode root = tree.root;
        table = new TreeWalker(root).collector;
    }

    public PrefixCodeTable(ObjectInputStream is) throws ClassNotFoundException, IOException {
        table = (Map<Character, CharEncoding>) is.readObject();
    }

    protected PrefixCodeTable() {
        table = new HashMap<>();
    }

    public CharEncoding get(char c) {
        CharEncoding charEncoding = table.get(c);
        if (charEncoding == null) {
            throw new RuntimeException("ERROR: no character encoding for character _" + c + "_");
        }
        return charEncoding;
    }
}
