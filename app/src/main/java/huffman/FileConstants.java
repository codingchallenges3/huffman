package huffman;

public interface FileConstants {
    int BUF_SIZE = 1024;
    int MAGIC_NUMBER = 0xBA;
    int END_OF_FILE_MARKER = 0xAB;
}
